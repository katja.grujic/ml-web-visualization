# ML-web-visualization

Application (dashboard) used to visualize network traffic parameters and machine learning models predictions for encrypted YouTube service

![Labels](labels.png)
![Features](features.png)
