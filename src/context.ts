import {createContext, useContext} from 'react';
import store, {RootState} from "./ducks/store";

const State = createContext(store);

export default State;

export function useAppState() {
  return useContext(State);
}
