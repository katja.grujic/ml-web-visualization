export interface ExportCsvFormat {
  stalling: string;
  bitrate: string;
  resolution: string;

  DLrate_w1: number;
  DLrate_wl5: number;
  DLrate_wl10: number;
  DLrate_wl20: number;
  DLrate_wl100: number;
  DLload_w1: number;
  DLload_wl5: number;
  DLload_wl10: number;
  DLload_wl20: number;
  DLload_wl100: number;
  ULnPckts_w1: number;
  ULnPckts_wl5: number;
  ULnPckts_wl10: number;
  ULnPckts_wl20: number;
  ULnPckts_wl100: number;
  ULavgSize_w1: number;
  ULavgSize_wl5: number;
  ULavgSize_wl10: number;
  ULavgSize_wl20: number;
  ULavgSize_wl100: number;
  ULstdSize_w1: number;
  ULstdSize_wl5: number;
  ULstdSize_wl10: number;
  ULstdSize_wl20: number;
  ULstdSize_wl100: number;
}