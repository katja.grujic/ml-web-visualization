import {Features} from "./Features";
import {Labels} from "./Labels";

export interface FeaturesLabels {
    features: Features;
    labels: Labels;
}