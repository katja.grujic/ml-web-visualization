export interface Labels {
    stalling: string;
    bitrate: string;
    resolution: string;
}