import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/App/App';
import {PersistGate} from "redux-persist/integration/react";
import {Provider} from 'react-redux';
import store, {persistor} from "./ducks/store";
import {Router} from "react-router";
import history from './history';
import * as serviceWorker from "./serviceWorker";
import 'react-tabs/style/react-tabs.css';
import "bootstrap/dist/css/bootstrap.min.css";

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA

    ReactDOM.render(
        <Provider store={store}>
            <PersistGate persistor={persistor} loading={<div> loading </div>}>
                <Router history={history}>
                    <App />
                </Router>
            </PersistGate>
        </Provider>,
        document.getElementById('root') as HTMLElement
    );

serviceWorker.unregister();