import * as React from "react";
import { ChangeEvent, useState } from "react";
import { Button, Col, Container, Row } from "reactstrap";
import styled from "styled-components";
import { useDispatch } from "react-redux";
import { dataActions } from "../../ducks/dataVisualization/actions";

const StyledContainer = styled(Container)`
  margin-top: 20%;
  grid-area: content;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 30px;
  box-shadow: 6px 6px 3px 0 rgba(209, 209, 209, 1);
  height: 250px;
  background-color: #eff0f6;
`;

const FormError = styled.p`
  color: red;
  margin-left: 5px;
  margin-top: 4px;
`;

const StyledButton = styled(Button)`
  margin-top: 20px;
  background-color: #64b8d6 !important;
  color: black !important;
  font-weight: bold !important;
  &:disabled {
    background-color: rgb(230, 230, 230) !important;
  }
`;

const MAX_FILE_SIZE = 31457280;

const CsvUpload = () => {
  const dispatch = useDispatch();

  const [file, setFile] = useState();
  const [error, setError] = useState(false);

  const handleBrowse = (event: ChangeEvent<HTMLInputElement>) => {
    if (event.target.files && event.target.files[0]) {
      setError(false);
      if (event.target.files[0].size > MAX_FILE_SIZE) {
        setError(true);
      }
      setFile(event.target.files[0]);
    } else {
      setError(false);
      setFile(undefined);
    }
  };

  const handleUpload = (file: File) => {
    dispatch(dataActions.uploadDataRequest(file));
  };

  return (
    <StyledContainer>
      <Row>
        <Col xs="2"/>
        <Col xs="8">
          <p style={{fontSize:"25px"}}>Upload .csv file: </p>
          <input
            id="csvFile"
            type="file"
            accept=".csv"
            onChange={handleBrowse}
          />
          <FormError>{error ? "File too large" : ""}</FormError>
          <StyledButton
            disabled={file && !error ? false : true}
            onClick={() => handleUpload(file)}
          >
            Start visualization
          </StyledButton>
        </Col>
        <Col xs="2"/>
      </Row>
    </StyledContainer>
  );
};

export default CsvUpload;
