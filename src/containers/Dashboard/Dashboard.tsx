import * as React from "react";
import LabelsChart from "../Charts/LabelsChart";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { useState } from "react";
import { dataActions } from "../../ducks/dataVisualization/actions";
import { Button, Col, Container, Row } from "reactstrap";
import { Tab, TabList, TabPanel, Tabs } from "react-tabs";
import { PrimaryTabs, SecondaryTabs } from "../../styles/PrimaryTabs";
import {
  getCsvData,
  getCsvDataInTimeFrame, getCurrentTime, getLabelBitrateLast, getLabelResolutionLast, getLabelStallingLast,
  getTimeFrame
} from "../../ducks/dataVisualization/selectors";
import Select from "react-select";
import { CSVLink } from "react-csv";
import { createNumberArray } from "../../utils/createNumberArray";
import {
  getFeatureDLload_w1InTimeFrame,
  getFeatureDLload_wl100InTimeFrame,
  getFeatureDLload_wl10InTimeFrame,
  getFeatureDLload_wl20InTimeFrame,
  getFeatureDLload_wl5InTimeFrame
} from "../../ducks/dataVisualization/selectors/selectDLload";
import {
  getFeatureDLrate_w1InTimeFrame,
  getFeatureDLrate_wl100InTimeFrame,
  getFeatureDLrate_wl10InTimeFrame,
  getFeatureDLrate_wl20InTimeFrame,
  getFeatureDLrate_wl5InTimeFrame
} from "../../ducks/dataVisualization/selectors/selectDLrate";
import {
  getFeatureULavgSize_w1InTimeFrame,
  getFeatureULavgSize_wl100InTimeFrame,
  getFeatureULavgSize_wl10InTimeFrame,
  getFeatureULavgSize_wl20InTimeFrame,
  getFeatureULavgSize_wl5InTimeFrame
} from "../../ducks/dataVisualization/selectors/selectULavgSize";
import {
  getFeatureULnPckts_w1InTimeFrame,
  getFeatureULnPckts_wl100InTimeFrame,
  getFeatureULnPckts_wl10InTimeFrame,
  getFeatureULnPckts_wl20InTimeFrame,
  getFeatureULnPckts_wl5InTimeFrame
} from "../../ducks/dataVisualization/selectors/selectULnPckts";
import {
  getFeatureULstdSize_w1InTimeFrame,
  getFeatureULstdSize_wl100InTimeFrame,
  getFeatureULstdSize_wl10InTimeFrame,
  getFeatureULstdSize_wl20InTimeFrame,
  getFeatureULstdSize_wl5InTimeFrame
} from "../../ducks/dataVisualization/selectors/selectULstdSize";
import FeatureChart from "../Charts/FeatureChart";
import { useInterval } from "../../utils/useInterval";

const Content = styled.div`
  grid-area: content;
  display: flex;
  align-items: center;
  justify-content: center;
  vertical-align: center;
  color: #bfbfbf;
`;

const StyledContainer = styled(Container)`
  margin-top: 2%;
`;

const StyledP = styled.p`
  font-size: 35px;
`;

const StyledButton = styled(Button)`
  margin-top: 10px;
  width: 90%;
  background-color: #d6d6d6 !important;
  color: black !important;
  font-weight: bold !important;
  &:hover {
    color: #2684ff !important;
  }
`;

const options = [
  { value: 10, label: "10 sec" },
  { value: 30, label: "30 sec" },
  { value: 60, label: "1 min" }
];

const Dashboard = () => {
  const [isRunning, setIsRunning] = useState(true);
  const dispatch = useDispatch();

  useInterval(() => {
    dispatch(dataActions.fetchDataRequest());
  }, isRunning ? 1000 : null);

  const handleChange = (selectedOption: any) => {
    dispatch(dataActions.changeTimeFrame(selectedOption.value + 1));
  };

  const handleIsRunning = () => {
    setIsRunning(!isRunning);
  };

  const timeFrame = useSelector(getTimeFrame);
  const currentTime = useSelector(getCurrentTime);

  let timeData;
  if (currentTime >= timeFrame) {
    timeData = createNumberArray(1 + currentTime - timeFrame, 1 + currentTime);
  } else {
    timeData = createNumberArray(1, 1 + currentTime);
  }

  const csvData = useSelector(getCsvData);
  const csvDataInTimeFrame = useSelector(getCsvDataInTimeFrame);

  const bitrateLast = useSelector(getLabelBitrateLast);
  const resolutionLast = useSelector(getLabelResolutionLast);
  const stallingLast = useSelector(getLabelStallingLast);

  const bitrateLastToShow = bitrateLast ? bitrateLast : "-";
  const resolutionLastToShow =  resolutionLast ? resolutionLast : "-";
  const stallingLastToShow = stallingLast ? stallingLast : "-";

  const DLload_w1_data = useSelector(getFeatureDLload_w1InTimeFrame);
  const DLload_wl5_data = useSelector(getFeatureDLload_wl5InTimeFrame);
  const DLload_wl10_data = useSelector(getFeatureDLload_wl10InTimeFrame);
  const DLload_wl20_data = useSelector(getFeatureDLload_wl20InTimeFrame);
  const DLload_wl100_data = useSelector(getFeatureDLload_wl100InTimeFrame);

  const DLrate_w1_data = useSelector(getFeatureDLrate_w1InTimeFrame);
  const DLrate_wl5_data = useSelector(getFeatureDLrate_wl5InTimeFrame);
  const DLrate_wl10_data = useSelector(getFeatureDLrate_wl10InTimeFrame);
  const DLrate_wl20_data = useSelector(getFeatureDLrate_wl20InTimeFrame);
  const DLrate_wl100_data = useSelector(getFeatureDLrate_wl100InTimeFrame);

  const ULavgSize_w1_data = useSelector(getFeatureULavgSize_w1InTimeFrame);
  const ULavgSize_wl5_data = useSelector(getFeatureULavgSize_wl5InTimeFrame);
  const ULavgSize_wl10_data = useSelector(getFeatureULavgSize_wl10InTimeFrame);
  const ULavgSize_wl20_data = useSelector(getFeatureULavgSize_wl20InTimeFrame);
  const ULavgSize_wl100_data = useSelector(getFeatureULavgSize_wl100InTimeFrame);

  const ULnPckts_w1_data = useSelector(getFeatureULnPckts_w1InTimeFrame);
  const ULnPckts_wl5_data = useSelector(getFeatureULnPckts_wl5InTimeFrame);
  const ULnPckts_wl10_data = useSelector(getFeatureULnPckts_wl10InTimeFrame);
  const ULnPckts_wl20_data = useSelector(getFeatureULnPckts_wl20InTimeFrame);
  const ULnPckts_wl100_data = useSelector(getFeatureULnPckts_wl100InTimeFrame);

  const ULstdSize_w1_data = useSelector(getFeatureULstdSize_w1InTimeFrame);
  const ULstdSize_wl5_data = useSelector(getFeatureULstdSize_wl5InTimeFrame);
  const ULstdSize_wl10_data = useSelector(getFeatureULstdSize_wl10InTimeFrame);
  const ULstdSize_wl20_data = useSelector(getFeatureULstdSize_wl20InTimeFrame);
  const ULstdSize_wl100_data = useSelector(getFeatureULstdSize_wl100InTimeFrame);

  return (
    <>
      <StyledContainer fluid>
        <Row>
          <Col xs="2">
            <div style={{ textAlign: "right" }}>
              <StyledP style={{ marginTop: "105px" }}>Bitrate</StyledP>
              <StyledP style={{ marginTop: "80px" }}>Resolution</StyledP>
              <StyledP style={{ marginTop: "85px" }}>Stalling</StyledP>
            </div>
          </Col>
          <Col xs="8">
            <LabelsChart />
          </Col>
          <Col xs="1">
            <StyledP style={{ marginTop: "100px" }}>{bitrateLastToShow.toUpperCase()}</StyledP>
            <StyledP style={{ marginTop: "80px" }}>{resolutionLastToShow.toUpperCase()}</StyledP>
            <StyledP style={{ marginTop: "85px" }}>{stallingLastToShow.toUpperCase()}</StyledP>
            <br />
            <Select
              value={options.find(option => option.value === timeFrame)}
              onChange={handleChange}
              options={options}
            />
          </Col>
          <Col xs="1">
            <div style={{ position: "absolute", bottom: "0" }}>
              <StyledButton onClick={() => handleIsRunning()}>{isRunning ? "Pause" : "Continue"}</StyledButton>
              <CSVLink data={csvData} filename="all-labels-features.csv">
                <StyledButton>Export all</StyledButton>
              </CSVLink>
              <CSVLink data={csvDataInTimeFrame} filename="current-labels-features.csv">
                <StyledButton>Export currently shown</StyledButton>
              </CSVLink>
            </div>
          </Col>
        </Row>
      </StyledContainer>

      <Content>
        <PrimaryTabs>
          <Tabs>
            <TabList>
              <Tab>By Most Relevant</Tab>
              <Tab>By Feature</Tab>
            </TabList>

            <TabPanel>
              <SecondaryTabs>
                <Tabs>
                  <TabList>
                    <Tab>Bitrate</Tab>
                    <Tab>Resolution</Tab>
                    <Tab>Stalling</Tab>
                  </TabList>

                  <TabPanel>
                    <FeatureChart name="DLrate_wl100" xAxisData={timeData} data={DLrate_wl100_data} />
                    <FeatureChart name="DLrate_wl20" xAxisData={timeData} data={DLrate_wl20_data} />
                    <FeatureChart name="DLload_wl100" xAxisData={timeData} data={DLload_wl100_data} />
                    <FeatureChart name="DLrate_wl10" xAxisData={timeData} data={DLrate_wl10_data} />
                    <FeatureChart name="DLrate_wl5" xAxisData={timeData} data={DLrate_wl5_data} />
                    <FeatureChart name="ULnPckts_wl100" xAxisData={timeData} data={ULnPckts_wl100_data} />
                  </TabPanel>

                  <TabPanel>
                    <FeatureChart name="DLload_wl100" xAxisData={timeData} data={DLload_wl100_data} />
                    <FeatureChart name="DLrate_wl100" xAxisData={timeData} data={DLrate_wl100_data} />
                    <FeatureChart name="ULavgSize_wl100" xAxisData={timeData} data={ULavgSize_wl100_data} />
                    <FeatureChart name="DLload_wl10" xAxisData={timeData} data={DLload_wl10_data} />
                    <FeatureChart name="ULstdSize_wl100" xAxisData={timeData} data={ULstdSize_wl100_data} />
                    <FeatureChart name="DLrate_wl20" xAxisData={timeData} data={DLrate_wl20_data} />
                  </TabPanel>

                  <TabPanel>
                    <FeatureChart name="DLload_wl10" xAxisData={timeData} data={DLload_wl10_data} />
                    <FeatureChart name="ULnPckts_wl20" xAxisData={timeData} data={ULnPckts_wl20_data} />
                    <FeatureChart name="DLrate_w1" xAxisData={timeData} data={DLrate_w1_data} />
                    <FeatureChart name="DLload_wl20" xAxisData={timeData} data={DLload_wl20_data} />
                    <FeatureChart name="DLrate_wl5" xAxisData={timeData} data={DLrate_wl5_data} />
                    <FeatureChart name="DLrate_wl10" xAxisData={timeData} data={DLrate_wl10_data} />
                  </TabPanel>

                </Tabs>
              </SecondaryTabs>
            </TabPanel>

            <TabPanel>
              <SecondaryTabs>
                <Tabs>
                  <TabList>
                    <Tab>DL Load</Tab>
                    <Tab>DL Rate</Tab>
                    <Tab>UL n Packets</Tab>
                    <Tab>UL avg Size</Tab>
                    <Tab>UL std Size</Tab>
                  </TabList>

                  <TabPanel>
                    <FeatureChart name="DLload_w1" xAxisData={timeData} data={DLload_w1_data} />
                    <FeatureChart name="DLload_wl5" xAxisData={timeData} data={DLload_wl5_data} />
                    <FeatureChart name="DLload_wl10" xAxisData={timeData} data={DLload_wl10_data} />
                    <FeatureChart name="DLload_wl20" xAxisData={timeData} data={DLload_wl20_data} />
                    <FeatureChart name="DLload_wl100" xAxisData={timeData} data={DLload_wl100_data} />
                  </TabPanel>

                  <TabPanel>
                    <FeatureChart name="DLrate_w1" xAxisData={timeData} data={DLrate_w1_data} />
                    <FeatureChart name="DLrate_wl5" xAxisData={timeData} data={DLrate_wl5_data} />
                    <FeatureChart name="DLrate_wl10" xAxisData={timeData} data={DLrate_wl10_data} />
                    <FeatureChart name="DLrate_wl20" xAxisData={timeData} data={DLrate_wl20_data} />
                    <FeatureChart name="DLrate_wl100" xAxisData={timeData} data={DLrate_wl100_data} />
                  </TabPanel>

                  <TabPanel>
                    <FeatureChart name="ULnPckts_w1" xAxisData={timeData} data={ULnPckts_w1_data} />
                    <FeatureChart name="ULnPckts_wl5" xAxisData={timeData} data={ULnPckts_wl5_data} />
                    <FeatureChart name="ULnPckts_wl10" xAxisData={timeData} data={ULnPckts_wl10_data} />
                    <FeatureChart name="ULnPckts_wl20" xAxisData={timeData} data={ULnPckts_wl20_data} />
                    <FeatureChart name="ULnPckts_wl100" xAxisData={timeData} data={ULnPckts_wl100_data} />
                  </TabPanel>

                  <TabPanel>
                    <FeatureChart name="ULavgSize_w1" xAxisData={timeData} data={ULavgSize_w1_data} />
                    <FeatureChart name="ULavgSize_wl5" xAxisData={timeData} data={ULavgSize_wl5_data} />
                    <FeatureChart name="ULavgSize_wl10" xAxisData={timeData} data={ULavgSize_wl10_data} />
                    <FeatureChart name="ULavgSize_wl20" xAxisData={timeData} data={ULavgSize_wl20_data} />
                    <FeatureChart name="ULavgSize_wl100" xAxisData={timeData} data={ULavgSize_wl100_data} />
                  </TabPanel>

                  <TabPanel>
                    <FeatureChart name="ULstdSize_w1" xAxisData={timeData} data={ULstdSize_w1_data} />
                    <FeatureChart name="ULstdSize_wl5" xAxisData={timeData} data={ULstdSize_wl5_data} />
                    <FeatureChart name="ULstdSize_wl10" xAxisData={timeData} data={ULstdSize_wl10_data} />
                    <FeatureChart name="ULstdSize_wl20" xAxisData={timeData} data={ULstdSize_wl20_data} />
                    <FeatureChart name="ULstdSize_wl100" xAxisData={timeData} data={ULstdSize_wl100_data} />
                  </TabPanel>

                </Tabs>
              </SecondaryTabs>
            </TabPanel>
          </Tabs>
        </PrimaryTabs>
      </Content>
    </>
  );
};

export default Dashboard;
