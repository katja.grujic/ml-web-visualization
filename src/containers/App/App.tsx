import React from "react";
import { Route, Switch } from "react-router";
import Dashboard from "../Dashboard/Dashboard";
import CsvUpload from "../FileUpload/CsvUpload";

const App: React.FC = () => {
  return (
    <React.Fragment>
      <Switch>
        <Route path="/dashboard" component={Dashboard} />
        <Route path="/" component={CsvUpload} />
      </Switch>
    </React.Fragment>
  );
};

export default App;
