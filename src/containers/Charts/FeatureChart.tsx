import * as React from "react";
import ReactEcharts from "echarts-for-react";
import styled from "styled-components";

const ChartContainer = styled.div`
  width: 31%;
  height: 100%;
  border: 1px solid #bcbcbc;
  border-radius: 10px;
  margin: 15px;
  display: inline-block;
`;

interface Props {
  name: string;
  xAxisData: number[];
  data: number[];
}

const FeatureChart = (props: Props) => {

  return (
    <ChartContainer>
      <ReactEcharts
        option={{
          title: {
            text: props.name
          },
          tooltip: {
            trigger: "axis"
          },
          toolbox: {
            feature: {
              saveAsImage: { title: "Save as image" }
            }
          },
          legend: {
            data: props.name
          },
          grid: {
            left: "3%",
            right: "4%",
            bottom: "3%",
            containLabel: true
          },
          xAxis: {
            type: "category",
            boundaryGap: false,
            data: props.xAxisData
          },
          yAxis: {
            type: "value"
          },
          series: [
            {
              name: props.name,
              type: "line",
              data: props.data
            }
          ]
        }}
      />
    </ChartContainer>
  );
};

export default FeatureChart;
