import * as React from "react";
import ReactEcharts from "echarts-for-react";
import styled from "styled-components";
import { useSelector } from "react-redux";
import {
  getCurrentTime,
  getLabelBitrateInTimeFrame,
  getLabelResolutionInTimeFrame,
  getLabelStallingInTimeFrame,
  getTimeFrame
} from "../../ducks/dataVisualization/selectors";
import { createNumberArray } from "../../utils/createNumberArray";

const ChartContainer = styled.div`
  width: 100%;
  height: 500px;
  border: 1px solid #bcbcbc;
  border-radius: 10px;
  padding-bottom: 15px;
  padding-top: 5px;
  margin: auto;
`;

const LabelsChart = () => {
   const bitrateData = useSelector(getLabelBitrateInTimeFrame);
   const resolutionData = useSelector(getLabelResolutionInTimeFrame);
   const stallingData = useSelector(getLabelStallingInTimeFrame);

  const yAxisData = ["no", "yes", "sd", "hd", "low", "high"];

  const timeFrame = useSelector(getTimeFrame);
  const currentTime = useSelector(getCurrentTime);

  let timeData;
  if (currentTime >= timeFrame) {
    timeData = createNumberArray(1 + currentTime - timeFrame, 1 + currentTime);
  } else {
    timeData = createNumberArray(1, 1 + currentTime);
  }

  return (
    <ChartContainer>
      <ReactEcharts
        style={{ height: "100%" }}
        option={{
          tooltip: {
            enabled: false
          },
          legend: {
            data: ["Bitrate", "Resolution", "Stalling"]
          },
          grid: { height: "400px" },
          toolbox: {
            feature: {
              saveAsImage: { title: "Save as image" }
            }
          },
          xAxis: {
            type: "category",
            boundaryGap: false,
            data: timeData
          },
          yAxis: {
            type: "category",
            data: yAxisData
          },
          series: [
            {
              name: "Bitrate",
              type: "line",
              step: "start",
              lineStyle: { width: 4 },
              data: bitrateData
            },
            {
              name: "Resolution",
              type: "line",
              step: "start",
              lineStyle: { width: 4 },
              data: resolutionData
            },
            {
              name: "Stalling",
              type: "line",
              step: "start",
              lineStyle: { width: 4 },
              data: stallingData
            }
          ]
        }}
      />
    </ChartContainer>
  );
};

export default LabelsChart;
