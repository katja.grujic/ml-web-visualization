import styled from "styled-components";

export const PrimaryTabs = styled.div`
  padding-bottom: 50px;
  width: 100%;
  margin: 20px;
  .react-tabs__tab--selected {
    border-color: #2684FF;
    color: #2684FF;
    font-weight: bold;
  }
`;

export const SecondaryTabs = styled.div`
  padding-left: 40px;
  margin-top: 40px;
  .react-tabs__tab--selected {
    border-color: #2684FF;
    color: #2684FF;
    font-weight: bold;
  }
`;