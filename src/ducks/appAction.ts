import { ActionType } from "typesafe-actions";
import {dataActions} from "./dataVisualization/actions";

export type AppAction = ActionType<typeof dataActions>;
