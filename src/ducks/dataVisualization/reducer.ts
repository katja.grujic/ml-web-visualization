import { getType } from "typesafe-actions";
import produce from "immer";
import { dataActions } from "./actions";
import { ExportCsvFormat } from "../../types/dataVisualization/ExportCsvFormat";

export interface DataState {
  timeFrame: number;
  currentTime: number;
  csvData: ExportCsvFormat[];

  stalling: string[];
  bitrate: string[];
  resolution: string[];

  DLrate_w1: number[];
  DLrate_wl5: number[];
  DLrate_wl10: number[];
  DLrate_wl20: number[];
  DLrate_wl100: number[];
  DLload_w1: number[];
  DLload_wl5: number[];
  DLload_wl10: number[];
  DLload_wl20: number[];
  DLload_wl100: number[];
  ULnPckts_w1: number[];
  ULnPckts_wl5: number[];
  ULnPckts_wl10: number[];
  ULnPckts_wl20: number[];
  ULnPckts_wl100: number[];
  ULavgSize_w1: number[];
  ULavgSize_wl5: number[];
  ULavgSize_wl10: number[];
  ULavgSize_wl20: number[];
  ULavgSize_wl100: number[];
  ULstdSize_w1: number[];
  ULstdSize_wl5: number[];
  ULstdSize_wl10: number[];
  ULstdSize_wl20: number[];
  ULstdSize_wl100: number[];
}

export const initialState: DataState = {
  timeFrame: 11,
  currentTime: 0,
  csvData: [],

  stalling: [],
  bitrate: [],
  resolution: [],

  DLrate_w1: [],
  DLrate_wl5: [],
  DLrate_wl10: [],
  DLrate_wl20: [],
  DLrate_wl100: [],
  DLload_w1: [],
  DLload_wl5: [],
  DLload_wl10: [],
  DLload_wl20: [],
  DLload_wl100: [],
  ULnPckts_w1: [],
  ULnPckts_wl5: [],
  ULnPckts_wl10: [],
  ULnPckts_wl20: [],
  ULnPckts_wl100: [],
  ULavgSize_w1: [],
  ULavgSize_wl5: [],
  ULavgSize_wl10: [],
  ULavgSize_wl20: [],
  ULavgSize_wl100: [],
  ULstdSize_w1: [],
  ULstdSize_wl5: [],
  ULstdSize_wl10: [],
  ULstdSize_wl20: [],
  ULstdSize_wl100: []
};

export const dataReducer = produce((state: DataState, action) => {
  switch (action.type) {
    case getType(dataActions.changeTimeFrame):
      state.timeFrame = action.payload;
      return;
    case getType(dataActions.fetchDataSuccess):
      state.currentTime = state.currentTime + 1;
      const newCsvData = {...action.payload.labels, ...action.payload.features};
      state.csvData = state.csvData.concat(newCsvData);

      state.stalling = state.stalling.concat(action.payload.labels.stalling);
      state.bitrate = state.bitrate.concat(action.payload.labels.bitrate);
      state.resolution = state.resolution.concat(action.payload.labels.resolution);

      state.DLrate_w1 = state.DLrate_w1.concat(action.payload.features.DLrate_w1);
      state.DLrate_wl5 = state.DLrate_wl5.concat(action.payload.features.DLrate_wl5);
      state.DLrate_wl10 = state.DLrate_wl10.concat(action.payload.features.DLrate_wl10);
      state.DLrate_wl20 = state.DLrate_wl20.concat(action.payload.features.DLrate_wl20);
      state.DLrate_wl100 = state.DLrate_wl100.concat(action.payload.features.DLrate_wl100);

      state.DLload_w1 = state.DLload_w1.concat(action.payload.features.DLload_w1);
      state.DLload_wl5 = state.DLload_wl5.concat(action.payload.features.DLload_wl5);
      state.DLload_wl10 = state.DLload_wl10.concat(action.payload.features.DLload_wl10);
      state.DLload_wl20 = state.DLload_wl20.concat(action.payload.features.DLload_wl20);
      state.DLload_wl100 = state.DLload_wl100.concat(action.payload.features.DLload_wl100);

      state.ULnPckts_w1 = state.ULnPckts_w1.concat(action.payload.features.ULnPckts_w1);
      state.ULnPckts_wl5 = state.ULnPckts_wl5.concat(action.payload.features.ULnPckts_wl5);
      state.ULnPckts_wl10 = state.ULnPckts_wl10.concat(action.payload.features.ULnPckts_wl10);
      state.ULnPckts_wl20 = state.ULnPckts_wl20.concat(action.payload.features.ULnPckts_wl20);
      state.ULnPckts_wl100 = state.ULnPckts_wl100.concat(action.payload.features.ULnPckts_wl100);

      state.ULavgSize_w1 = state.ULavgSize_w1.concat(action.payload.features.ULavgSize_w1);
      state.ULavgSize_wl5 = state.ULavgSize_wl5.concat(action.payload.features.ULavgSize_wl5);
      state.ULavgSize_wl10 = state.ULavgSize_wl10.concat(action.payload.features.ULavgSize_wl10);
      state.ULavgSize_wl20 = state.ULavgSize_wl20.concat(action.payload.features.ULavgSize_wl20);
      state.ULavgSize_wl100 = state.ULavgSize_wl100.concat(action.payload.features.ULavgSize_wl100);

      state.ULstdSize_w1 = state.ULstdSize_w1.concat(action.payload.features.ULstdSize_w1);
      state.ULstdSize_wl5 = state.ULstdSize_wl5.concat(action.payload.features.ULstdSize_wl5);
      state.ULstdSize_wl10 = state.ULstdSize_wl10.concat(action.payload.features.ULstdSize_wl10);
      state.ULstdSize_wl20 = state.ULstdSize_wl20.concat(action.payload.features.ULstdSize_wl20);
      state.ULstdSize_wl100 = state.ULstdSize_wl100.concat(action.payload.features.ULstdSize_wl100);
      return;
    case getType(dataActions.uploadDataSuccess):
      state.timeFrame = 11;
      state.currentTime = 0;
      state.csvData = [];

      state.stalling = [];
      state.bitrate = [];
      state.resolution = [];

      state.DLrate_w1 = [];
      state.DLrate_wl5 = [];
      state.DLrate_wl10 = [];
      state.DLrate_wl20 = [];
      state.DLrate_wl100 = [];

      state.DLload_w1 = [];
      state.DLload_wl5 = [];
      state.DLload_wl10 = [];
      state.DLload_wl20 = [];
      state.DLload_wl100 = [];

      state.ULnPckts_w1 = [];
      state.ULnPckts_wl5 = [];
      state.ULnPckts_wl10 = [];
      state.ULnPckts_wl20 = [];
      state.ULnPckts_wl100 = [];

      state.ULavgSize_w1 = [];
      state.ULavgSize_wl5 = [];
      state.ULavgSize_wl10 = [];
      state.ULavgSize_wl20 = [];
      state.ULavgSize_wl100 = [];

      state.ULstdSize_w1 = [];
      state.ULstdSize_wl5 = [];
      state.ULstdSize_wl10 = [];
      state.ULstdSize_wl20 = [];
      state.ULstdSize_wl100 = [];
      return;
    default:
      return;
  }
}, initialState);
