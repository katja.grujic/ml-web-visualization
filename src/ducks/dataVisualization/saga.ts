import {dataActions} from "./actions";
import { getType } from "typesafe-actions";
import { all, call, put, takeEvery } from "redux-saga/effects";
import history from "../../history";
import { fetchData, uploadData } from "./api";
import { toast } from "react-toastify";

function* uploadDataRequestWorker(action: ReturnType<typeof dataActions.uploadDataRequest>) {
  try {
    yield call(uploadData, action.payload);
    yield put(dataActions.uploadDataSuccess());
    history.push("/dashboard")
  } catch (e) {
    console.log(e);
    toast(`Server could not process given file!`, { type: toast.TYPE.ERROR });
    yield put(dataActions.uploadDataFailed());
  }
}

function* fetchDataRequestWorker(action: ReturnType<typeof dataActions.fetchDataRequest>) {
    try {
        const featuresLabels = yield call(fetchData);
        yield put(dataActions.fetchDataSuccess(featuresLabels));
    } catch (e) {
        console.log(e);
    }
}

function* dataSagaWatcher() {
    yield all([takeEvery(getType(dataActions.fetchDataRequest), fetchDataRequestWorker)]);
    yield all([takeEvery(getType(dataActions.uploadDataRequest), uploadDataRequestWorker)]);
}

export default dataSagaWatcher;
