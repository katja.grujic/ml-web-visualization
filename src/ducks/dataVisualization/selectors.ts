import {RootState} from "../store";
import _ from "lodash";
import { createSelector } from "reselect";

export const getTimeFrame = (state: RootState) => state.data.timeFrame;
export const getCurrentTime = (state: RootState) => state.data.currentTime;
export const getCsvData = (state: RootState) => state.data.csvData;

export const getLabelStalling = (state: RootState) => state.data.stalling;
export const getLabelBitrate = (state: RootState) => state.data.bitrate;
export const getLabelResolution = (state: RootState) => state.data.resolution;

export const getFeatureDLrate_w1 = (state: RootState) => state.data.DLrate_w1;
export const getFeatureDLrate_wl5 = (state: RootState) => state.data.DLrate_wl5;
export const getFeatureDLrate_wl10 = (state: RootState) => state.data.DLrate_wl10;
export const getFeatureDLrate_wl20 = (state: RootState) => state.data.DLrate_wl20;
export const getFeatureDLrate_wl100 = (state: RootState) => state.data.DLrate_wl100;

export const getFeatureDLload_w1 = (state: RootState) => state.data.DLload_w1;
export const getFeatureDLload_wl5 = (state: RootState) => state.data.DLload_wl5;
export const getFeatureDLload_wl10 = (state: RootState) => state.data.DLload_wl10;
export const getFeatureDLload_wl20 = (state: RootState) => state.data.DLload_wl20;
export const getFeatureDLload_wl100 = (state: RootState) => state.data.DLload_wl100;

export const getFeatureULnPckts_w1 = (state: RootState) => state.data.ULnPckts_w1;
export const getFeatureULnPckts_wl5 = (state: RootState) => state.data.ULnPckts_wl5;
export const getFeatureULnPckts_wl10 = (state: RootState) => state.data.ULnPckts_wl10;
export const getFeatureULnPckts_wl20 = (state: RootState) => state.data.ULnPckts_wl20;
export const getFeatureULnPckts_wl100 = (state: RootState) => state.data.ULnPckts_wl100;

export const getFeatureULavgSize_w1 = (state: RootState) => state.data.ULavgSize_w1;
export const getFeatureULavgSize_wl5 = (state: RootState) => state.data.ULavgSize_wl5;
export const getFeatureULavgSize_wl10 = (state: RootState) => state.data.ULavgSize_wl10;
export const getFeatureULavgSize_wl20 = (state: RootState) => state.data.ULavgSize_wl20;
export const getFeatureULavgSize_wl100 = (state: RootState) => state.data.ULavgSize_wl100;

export const getFeatureULstdSize_w1 = (state: RootState) => state.data.ULstdSize_w1;
export const getFeatureULstdSize_wl5 = (state: RootState) => state.data.ULstdSize_wl5;
export const getFeatureULstdSize_wl10 = (state: RootState) => state.data.ULstdSize_wl10;
export const getFeatureULstdSize_wl20 = (state: RootState) => state.data.ULstdSize_wl20;
export const getFeatureULstdSize_wl100 = (state: RootState) => state.data.ULstdSize_wl100;

export const getCsvDataInTimeFrame = createSelector(
  getCsvData,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);

export const getLabelStallingLast = createSelector(
  getLabelStalling,
  (data) => {
    return data.slice(-1)[0];
  }
);

export const getLabelStallingInTimeFrame = createSelector(
  getLabelStalling,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);

export const getLabelBitrateLast = createSelector(
  getLabelBitrate,
  (data) => {
    return data.slice(-1)[0];
  }
);

export const getLabelBitrateInTimeFrame = createSelector(
  getLabelBitrate,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);

export const getLabelResolutionLast = createSelector(
  getLabelResolution,
  (data) => {
    return data.slice(-1)[0];
  }
);

export const getLabelResolutionInTimeFrame = createSelector(
  getLabelResolution,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);