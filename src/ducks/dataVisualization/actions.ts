import { createAction } from "typesafe-actions";
import { FeaturesLabels } from "../../types/dataVisualization/FeaturesLabels";

export const dataActions = {
  uploadDataRequest: createAction("UPLOAD_DATA_REQUEST")<File>(),
  uploadDataSuccess: createAction("UPLOAD_DATA_SUCCESS")<void>(),
  uploadDataFailed: createAction("UPLOAD_DATA_FAILED")<void>(),

  fetchDataRequest: createAction("FETCH_DATA_REQUEST")<void>(),
  fetchDataSuccess: createAction("FETCH_DATA_SUCCESS")<FeaturesLabels>(),

  changeTimeFrame: createAction("CHANGE_TIME_FRAME")<number>()
};
