import api from "../../utils/api";

export const uploadData = (data: File) => {
  const config = { headers: { 'Content-Type': 'multipart/form-data' } };
  const fd = new FormData();
  fd.append('csvFile', data);

  return api.post(`/data/upload`, fd, config).then(res => res.data);
};

export const fetchData = () =>
    api.get("/data/read").then(res => res.data);