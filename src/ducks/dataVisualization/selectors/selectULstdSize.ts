import { createSelector } from "reselect";
import _ from "lodash";
import {
  getFeatureULstdSize_w1,
  getFeatureULstdSize_wl10, getFeatureULstdSize_wl100,
  getFeatureULstdSize_wl20,
  getFeatureULstdSize_wl5,
  getTimeFrame
} from "../selectors";

export const getFeatureULstdSize_w1InTimeFrame = createSelector(
  getFeatureULstdSize_w1,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);

export const getFeatureULstdSize_wl5InTimeFrame = createSelector(
  getFeatureULstdSize_wl5,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);

export const getFeatureULstdSize_wl10InTimeFrame = createSelector(
  getFeatureULstdSize_wl10,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);

export const getFeatureULstdSize_wl20InTimeFrame = createSelector(
  getFeatureULstdSize_wl20,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);

export const getFeatureULstdSize_wl100InTimeFrame = createSelector(
  getFeatureULstdSize_wl100,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);
