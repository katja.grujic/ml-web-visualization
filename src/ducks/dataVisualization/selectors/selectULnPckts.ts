import { createSelector } from "reselect";
import _ from "lodash";
import {
  getFeatureULnPckts_w1,
  getFeatureULnPckts_wl10, getFeatureULnPckts_wl100,
  getFeatureULnPckts_wl20,
  getFeatureULnPckts_wl5,
  getTimeFrame
} from "../selectors";

export const getFeatureULnPckts_w1InTimeFrame = createSelector(
  getFeatureULnPckts_w1,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);

export const getFeatureULnPckts_wl5InTimeFrame = createSelector(
  getFeatureULnPckts_wl5,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);

export const getFeatureULnPckts_wl10InTimeFrame = createSelector(
  getFeatureULnPckts_wl10,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);

export const getFeatureULnPckts_wl20InTimeFrame = createSelector(
  getFeatureULnPckts_wl20,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);

export const getFeatureULnPckts_wl100InTimeFrame = createSelector(
  getFeatureULnPckts_wl100,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);