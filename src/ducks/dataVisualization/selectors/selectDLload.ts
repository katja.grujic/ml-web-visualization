import { createSelector } from "reselect";
import _ from "lodash";
import {
  getFeatureDLload_w1,
  getFeatureDLload_wl10, getFeatureDLload_wl100,
  getFeatureDLload_wl20,
  getFeatureDLload_wl5,
  getTimeFrame
} from "../selectors";

export const getFeatureDLload_w1InTimeFrame = createSelector(
  getFeatureDLload_w1,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);

export const getFeatureDLload_wl5InTimeFrame = createSelector(
  getFeatureDLload_wl5,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);

export const getFeatureDLload_wl10InTimeFrame = createSelector(
  getFeatureDLload_wl10,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);

export const getFeatureDLload_wl20InTimeFrame = createSelector(
  getFeatureDLload_wl20,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);

export const getFeatureDLload_wl100InTimeFrame = createSelector(
  getFeatureDLload_wl100,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);