import { createSelector } from "reselect";
import _ from "lodash";
import {
  getFeatureDLrate_w1,
  getFeatureDLrate_wl10, getFeatureDLrate_wl100,
  getFeatureDLrate_wl20,
  getFeatureDLrate_wl5,
  getTimeFrame
} from "../selectors";

export const getFeatureDLrate_w1InTimeFrame = createSelector(
  getFeatureDLrate_w1,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);

export const getFeatureDLrate_wl5InTimeFrame = createSelector(
  getFeatureDLrate_wl5,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);

export const getFeatureDLrate_wl10InTimeFrame = createSelector(
  getFeatureDLrate_wl10,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);

export const getFeatureDLrate_wl20InTimeFrame = createSelector(
  getFeatureDLrate_wl20,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);

export const getFeatureDLrate_wl100InTimeFrame = createSelector(
  getFeatureDLrate_wl100,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);