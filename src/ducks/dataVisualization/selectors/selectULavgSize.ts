import { createSelector } from "reselect";
import _ from "lodash";
import {
  getFeatureULavgSize_w1,
  getFeatureULavgSize_wl10, getFeatureULavgSize_wl100,
  getFeatureULavgSize_wl20,
  getFeatureULavgSize_wl5,
  getTimeFrame
} from "../selectors";

export const getFeatureULavgSize_w1InTimeFrame = createSelector(
  getFeatureULavgSize_w1,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);

export const getFeatureULavgSize_wl5InTimeFrame = createSelector(
  getFeatureULavgSize_wl5,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);

export const getFeatureULavgSize_wl10InTimeFrame = createSelector(
  getFeatureULavgSize_wl10,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);

export const getFeatureULavgSize_wl20InTimeFrame = createSelector(
  getFeatureULavgSize_wl20,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);

export const getFeatureULavgSize_wl100InTimeFrame = createSelector(
  getFeatureULavgSize_wl100,
  getTimeFrame,
  (data, timeframe) => {
    return _.takeRight(data, timeframe);
  }
);