import { all } from "redux-saga/effects";
import dataSagaWatcher from "./dataVisualization/saga";

export default function* rootSaga() {
  yield all([dataSagaWatcher()]);
}
