import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import { persistReducer, persistStore } from "redux-persist";
import sessionStorage from "redux-persist/lib/storage/session";
import createSagaMiddleware from "redux-saga";
import { AppAction } from "./appAction";
import rootSaga from "./rootSaga";
import {dataReducer, DataState} from "./dataVisualization/reducer";

const persistConfig = {
  key: "root",
  storage: sessionStorage,
  blacklist: []
};

export const rootReducer = combineReducers<AppState, AppAction>({
  data: dataReducer
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

const sagaMiddleware = createSagaMiddleware();

const middleware = [sagaMiddleware];
const composeEnhancers =
  ((window as any).__REDUX_DEVTOOLS_EXTENSION__ &&
    (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
  compose;

interface StoreEnhancerState {}

interface AppState extends StoreEnhancerState {
  data: DataState;
}

export interface RootState extends AppState {}

const store = createStore(
  persistedReducer,
  composeEnhancers(applyMiddleware(...middleware))
);

export const persistor = persistStore(store);
// then run the saga
sagaMiddleware.run(rootSaga);

export default store;
