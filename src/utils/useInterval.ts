import { useEffect } from "react";

export function useInterval(callback: any, delay: any) {
  useEffect(() => {
    if (delay !== null) {
      let id = setInterval(callback, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}