import axios from 'axios';

export const baseURL = process.env.REACT_APP_API_BASE_URL;

const api = axios.create({
    baseURL,
    timeout: 3000,
});

export default api;