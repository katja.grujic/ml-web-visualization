import _ from "lodash";

export const createNumberArray = (minValue: number, maxValue: number) => {
  //return Array.from({ length: maxValue - minValue + 1 }, (v, k) => k);
  return _.range(minValue, maxValue);
};
